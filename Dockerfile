FROM java:openjdk-8-jre

ADD asta-customs-output-actor-all.jar asta-customs-output-actor-all.jar

CMD java -jar asta-customs-output-actor-all.jar
