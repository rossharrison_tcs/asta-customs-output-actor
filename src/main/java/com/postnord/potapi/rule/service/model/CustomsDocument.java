package com.postnord.potapi.rule.service.model;

import java.util.Objects;

public class CustomsDocument {
    private boolean customsDocument;

    public CustomsDocument(boolean customsDocument) {
        this.customsDocument = customsDocument;
    }

    public boolean isCustomsDocument() {
        return customsDocument;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomsDocument that = (CustomsDocument) o;
        return customsDocument == that.customsDocument;
    }

    @Override
    public int hashCode() {
        return Objects.hash(customsDocument);
    }

    @Override
    public String toString() {
        if (customsDocument) {
            return "1";
        } else {
            return "0";
        }
    }
}
