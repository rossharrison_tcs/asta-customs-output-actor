package com.postnord.potapi.rule.service.model;

public enum ImportCategory {
    EU,
    NON_EU,
    UNKNOWN;
}
