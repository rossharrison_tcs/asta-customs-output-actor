package com.postnord.potapi.processor.internal;

import java.util.Objects;

public class ConsigneeInfoExists {
    private boolean exists ;

    public ConsigneeInfoExists(boolean exists) {
        this.exists = exists;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConsigneeInfoExists that = (ConsigneeInfoExists) o;
        return exists == that.exists;
    }

    @Override
    public int hashCode() {
        return Objects.hash(exists);
    }

    @Override
    public String toString() {
        if (exists) {
            return "1";
        } else {
            return "0";
        }
    }
}
