package com.postnord.potapi.processor.internal;

import com.postnord.potapi.model.types.PnItemId;
import com.postnord.potapi.model.types.TransactionId;
import com.postnord.potapi.model.types.Version;
import com.postnord.potapi.rule.service.model.CustomsClassification;

import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class CustomsOrder {
    private static final String TAB = "\t";

    private final PnItemId pnItemId;
    private Size size;
    private ConsigneeInfoExists consigneeInfoExists;
    private CustomsClassification classification;
    private final TransactionId transactionId;
    private final Version currentItemVersion;
    private final Version currentOrderVersion;

    public CustomsOrder(PnItemId pnItemId, Size size, ConsigneeInfoExists consigneeInfoExists,
                 CustomsClassification classification, TransactionId transactionId, Version currentItemVersion,
                 Version currentOrderVersion) {
        this.pnItemId = pnItemId;
        this.size = size;
        this.consigneeInfoExists = consigneeInfoExists;
        this.classification = classification;
        this.transactionId = transactionId;
        this.currentItemVersion = currentItemVersion;
        this.currentOrderVersion = currentOrderVersion;
    }

    PnItemId getPnItemId() {
        return pnItemId;
    }

    byte[] serialize() {
        return toString().getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomsOrder that = (CustomsOrder) o;
        return Objects.equals(pnItemId, that.pnItemId) &&
                size == that.size &&
                Objects.equals(consigneeInfoExists, that.consigneeInfoExists) &&
                Objects.equals(classification, that.classification) &&
                Objects.equals(transactionId, that.transactionId) &&
                Objects.equals(currentItemVersion, that.currentItemVersion) &&
                Objects.equals(currentOrderVersion, that.currentOrderVersion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pnItemId, size, consigneeInfoExists, classification, transactionId, currentItemVersion, currentOrderVersion);
    }

    @Override
    public String toString() {
        String pnItemId = "";
        String size = "";
        String consigneeInfoExists = "";
        String prepaidVat = "";
        String customsDocument = "";
        String commercialRecipient = "";
        String category = "";
        String transactionId = "";
        String currentItemVersion = "";
        String currentOrderVersion = "";

        if (this.pnItemId != null) {
            pnItemId = this.pnItemId.toString();
        }

        if (this.size != null) {
            size = this.size.toString();
        }

        if (this.consigneeInfoExists != null) {
            consigneeInfoExists = this.consigneeInfoExists.toString();
        }

        if (classification != null) {
            if (classification.getPrepaidVat() != null) {
                prepaidVat = classification.getPrepaidVat().toString();
            }

            if (classification.getCustomsDocument() != null) {
                customsDocument = classification.getCustomsDocument().toString();
            }

            if (classification.getCommercialRecipient() != null) {
                commercialRecipient = classification.getCommercialRecipient().toString();
            }

            if (classification.getCategory() != null) {
                category = classification.getCategory().toString();
            }
        }

        if (this.transactionId != null) {
            transactionId = this.transactionId.toString();
        }

        if (this.currentItemVersion != null) {
            currentItemVersion = this.currentItemVersion.toString();
        }

        if (this.currentOrderVersion != null) {
            currentOrderVersion = this.currentOrderVersion.toString();
        }

        return pnItemId + TAB + size + TAB + consigneeInfoExists + TAB + prepaidVat + TAB + customsDocument + TAB + commercialRecipient + TAB + category + TAB + transactionId + TAB + currentItemVersion + TAB + currentOrderVersion;
    }
}
