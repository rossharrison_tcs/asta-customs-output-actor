package com.postnord.potapi.rule.service.model;

import java.util.Objects;

public class CommercialRecipient {
    private boolean commercialRecipient;

    public CommercialRecipient(boolean commercialRecipient) {
        this.commercialRecipient = commercialRecipient;
    }

    public boolean isCommercialRecipient() {
        return commercialRecipient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommercialRecipient that = (CommercialRecipient) o;
        return commercialRecipient == that.commercialRecipient;
    }

    @Override
    public int hashCode() {
        return Objects.hash(commercialRecipient);
    }

    @Override
    public String toString() {
        if (commercialRecipient) {
            return "1";
        } else {
            return "0";
        }
    }
}
