package com.postnord.potapi.processor.internal;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;

public class CustomsOrderKeyMapper implements KeyValueMapper<String, CustomsOrder, KeyValue<String, CustomsOrder>> {
    @Override
    public KeyValue<String, CustomsOrder> apply(String unusedKey, CustomsOrder value) {
        String key = java.util.UUID.nameUUIDFromBytes(value.getPnItemId().getPnItemId().getBytes()).toString();
        return new KeyValue<>(key, value);
    }
}
