package com.postnord.potapi.rule.service.filter;

import com.postnord.potapi.model.Transaction;

public interface VATFilter {
    boolean shouldBeClearedForVAT(Transaction transaction);
}
