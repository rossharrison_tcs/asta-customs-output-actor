package com.postnord.potapi.rule.service.model;

public enum Category {
    RESTRICTED,
    CUSTOMS,
    VAT,
    GIFT,
    MANUAL_HANDLING,
    ORDINARY
}
