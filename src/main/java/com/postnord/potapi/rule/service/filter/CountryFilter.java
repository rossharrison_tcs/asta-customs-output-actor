package com.postnord.potapi.rule.service.filter;

import com.postnord.potapi.rule.model.CustomsRulesModel;

public interface CountryFilter {
    boolean shouldBeClearedInCustoms(CustomsRulesModel transaction);
}
