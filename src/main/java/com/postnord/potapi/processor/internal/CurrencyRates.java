package com.postnord.potapi.processor.internal;

import java.time.LocalDate;

public class CurrencyRates {
	String currencyCode;
	String covertedTo;
	double sEK;
	double rate;
	LocalDate activeFrom;
	/**
	 * @return the currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}
	/**
	 * @param currencyCode the currencyCode to set
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	/**
	 * @return the covertedTo
	 */
	public String getCovertedTo() {
		return covertedTo;
	}
	/**
	 * @param covertedTo the covertedTo to set
	 */
	public void setCovertedTo(String covertedTo) {
		this.covertedTo = covertedTo;
	}
	/**
	 * @return the sEK
	 */
	public double getsEK() {
		return sEK;
	}
	/**
	 * @param sEK the sEK to set
	 */
	public void setsEK(double sEK) {
		this.sEK = sEK;
	}
	/**
	 * @return the rate
	 */
	public double getRate() {
		return rate;
	}
	/**
	 * @param rate the rate to set
	 */
	public void setRate(double rate) {
		this.rate = rate;
	}
	/**
	 * @return the activeFrom
	 */
	public LocalDate getActiveFrom() {
		return activeFrom;
	}
	/**
	 * @param activeFrom the activeFrom to set
	 */
	public void setActiveFrom(LocalDate activeFrom) {
		this.activeFrom = activeFrom;
	}
	
}
