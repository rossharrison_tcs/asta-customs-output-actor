package com.postnord.potapi.processor;

import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.model.types.ServiceCode;
import com.postnord.potapi.rule.service.model.Category;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;

import com.postnord.potapi.rule.model.CustomsRulesModel;

public class CustomRulesToTransactionKeyMapper
		implements KeyValueMapper<String, CustomsRulesModel, KeyValue<String, CustomsRulesModel>> {

	@Override
	public KeyValue<String, CustomsRulesModel> apply(String key, CustomsRulesModel value) {
		String transactionId = value.getTransaction().getTransactionId().getTransactionId();
		return new KeyValue<>(transactionId, value);
	}


}
