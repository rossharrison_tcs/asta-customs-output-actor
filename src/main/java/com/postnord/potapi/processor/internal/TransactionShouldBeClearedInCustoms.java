package com.postnord.potapi.processor.internal;

import org.apache.kafka.streams.kstream.Predicate;

import com.postnord.potapi.rule.model.CustomsRulesModel;
import com.postnord.potapi.rule.service.filter.CountryFilter;

public class TransactionShouldBeClearedInCustoms implements Predicate<String, CustomsRulesModel> {
    private CountryFilter countryFilter;

    public TransactionShouldBeClearedInCustoms(CountryFilter countryFilter) {
        this.countryFilter = countryFilter;
    }

    @Override
    public boolean test(String unusedKey, CustomsRulesModel value) {
        return countryFilter.shouldBeClearedInCustoms(value);
    }
}
