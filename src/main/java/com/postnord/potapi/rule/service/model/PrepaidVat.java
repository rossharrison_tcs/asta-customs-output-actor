package com.postnord.potapi.rule.service.model;

import java.util.Objects;

public class PrepaidVat {
    private boolean prepaidVat;

    public PrepaidVat(boolean prepaidVat) {
        this.prepaidVat = prepaidVat;
    }

    public boolean isPrepaidVat() {
        return prepaidVat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrepaidVat that = (PrepaidVat) o;
        return prepaidVat == that.prepaidVat;
    }

    @Override
    public int hashCode() {
        return Objects.hash(prepaidVat);
    }

    @Override
    public String toString() {
        if (prepaidVat) {
            return "1";
        } else {
            return "0";
        }
    }
}
