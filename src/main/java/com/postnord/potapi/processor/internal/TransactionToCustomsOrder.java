package com.postnord.potapi.processor.internal;

import java.util.ArrayList;
import java.util.List;

import org.apache.kafka.streams.kstream.ValueMapper;

import com.postnord.potapi.model.Item;
import com.postnord.potapi.model.Order;
import com.postnord.potapi.model.Payload;
import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.model.types.ItemId;
import com.postnord.potapi.model.types.PnItemId;
import com.postnord.potapi.model.types.TransactionId;
import com.postnord.potapi.model.types.Version;
import com.postnord.potapi.rule.model.CustomsRulesModel;
import com.postnord.potapi.rule.service.filter.CountryFilter;
import com.postnord.potapi.rule.service.model.Category;
import com.postnord.potapi.rule.service.model.CommercialRecipient;
import com.postnord.potapi.rule.service.model.CustomsClassification;
import com.postnord.potapi.rule.service.model.CustomsDocument;
import com.postnord.potapi.rule.service.model.PrepaidVat;

public class TransactionToCustomsOrder implements ValueMapper<CustomsRulesModel, List<CustomsOrder>> {

    private CountryFilter countryFilter;

    public TransactionToCustomsOrder(CountryFilter countryFilter) {
        this.countryFilter = countryFilter;
    }

    @Override
    public List<CustomsOrder> apply(CustomsRulesModel customsRulesModel) {
        List<CustomsOrder> res = new ArrayList<>();
        Transaction value = customsRulesModel.getTransaction();
        if (countryFilter.shouldBeClearedInCustoms(customsRulesModel)) {
            TransactionId transactionId = value.getTransactionId();

            Payload bundle = value.getPayload();

            List<Order> orders = bundle.getOrders();

            for (Order order : orders) {
                Version currentVersion = order.getCurrentVersion();
                ConsigneeInfoExists consigneeInfoExists = getConsigneeInfoExists(customsRulesModel);

                List<ItemId> itemIds = order.getItemIds();
                for (ItemId itemId : itemIds) {
                    Item item = getItem(itemId, bundle);

                    if (item != null) {
                        PnItemId pnItemId = item.getPnItemId();
                        Version currentItemVersion = item.getCurrentVersion();
                        Size size = getSize(item);
                        CustomsClassification classification = getClassification(customsRulesModel);

                        CustomsOrder customsOrder = new CustomsOrder(pnItemId, size, consigneeInfoExists,
                                classification, transactionId, currentItemVersion, currentVersion);

                        res.add(customsOrder);
                    }
                }
            }
        }

        return res;
    }

    private Item getItem(ItemId itemId, Payload bundle) {
        List<Item> items = bundle.getItems();

        for (Item item : items) {
            if (item.getItemId().equals(itemId)) {
                return item;
            }
        }

        return null;
    }

    private ConsigneeInfoExists getConsigneeInfoExists(CustomsRulesModel customsRulesModel) {
        if (customsRulesModel.getReciverName() != null) {
            return new ConsigneeInfoExists(true);
        }
        return new ConsigneeInfoExists(true);
    }

    private Size getSize(Item item) {

        if(item.getWeight() != null && item.getWeight().getWeight() != null){
            Integer weight = item.getWeight().getWeight();
            if(weight <= 100){
                return Size.SMALL;
            }else{
                return Size.LARGE;
            }
        }else{
            return Size.UNKNOWN;
        }

    }

    private CustomsClassification getClassification(CustomsRulesModel customsRulesModel) {
        PrepaidVat prepaidVat = new PrepaidVat(customsRulesModel.isPrepaidValue());
        CustomsDocument customsDocument = new CustomsDocument(customsRulesModel.isDocument());
        CommercialRecipient commercialRecipient = new CommercialRecipient(customsRulesModel.isCommercialReceipient());

        Category category ;
        if(customsRulesModel.getServiceCode() != null &&
                customsRulesModel.getServiceCode().equalsIgnoreCase(Category.CUSTOMS.toString())){
            category = getCategory(customsRulesModel.getServiceCode());
        } else if (customsRulesModel.getTransaction().getPayload() != null &&
                customsRulesModel.getTransaction().getPayload().getItems()!= null &&
                !customsRulesModel.getTransaction().getPayload().getItems().isEmpty() &&
                customsRulesModel.getTransaction().getPayload().getItems().get(0).getServiceCodes() != null &&
                !customsRulesModel.getTransaction().getPayload().getItems().get(0).getServiceCodes().isEmpty()){
            category = getCategory(customsRulesModel.getTransaction().getPayload().getItems().get(0).getServiceCodes().get(0).getServiceCode());
        }else if(customsRulesModel.getServiceCode() != null &&
                customsRulesModel.getServiceCode().equalsIgnoreCase(Category.GIFT.toString())){
            category = getCategory(customsRulesModel.getServiceCode());
        }
        else{
            category = getCategory(customsRulesModel.getServiceCode() != null ? customsRulesModel.getServiceCode() : "VAT");
        }

        //Category category = getCategory(customsRulesModel.getServiceCode());
        return new CustomsClassification(prepaidVat, customsDocument, commercialRecipient, category);
    }

    private Category getCategory(String additionalServiceCode) {

        if (additionalServiceCode != null && additionalServiceCode.equalsIgnoreCase(Category.CUSTOMS.toString())) {
            return Category.CUSTOMS;
        } else if (additionalServiceCode != null && additionalServiceCode.equalsIgnoreCase(Category.GIFT.toString())) {
            return Category.GIFT;
        } else if (additionalServiceCode != null && additionalServiceCode.equalsIgnoreCase(Category.RESTRICTED.toString())) {
            return Category.RESTRICTED;
        }else if (additionalServiceCode != null && additionalServiceCode.equalsIgnoreCase(Category.MANUAL_HANDLING.toString())) {
            return Category.MANUAL_HANDLING;
        }else if (additionalServiceCode != null && additionalServiceCode.equalsIgnoreCase(Category.ORDINARY.toString())) {
            return Category.ORDINARY;
        }else {
            return Category.VAT;
        }
    }

}