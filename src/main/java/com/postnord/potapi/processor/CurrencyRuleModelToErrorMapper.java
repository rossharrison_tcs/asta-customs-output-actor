package com.postnord.potapi.processor;

import com.postnord.potapi.rule.model.CustomsRulesModel;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;

public class CurrencyRuleModelToErrorMapper implements KeyValueMapper<String, CustomsRulesModel, KeyValue<String, String>> {
    @Override
    public KeyValue<String, String> apply(String key, CustomsRulesModel value) {
        return new KeyValue<>(value.getCurrencyCode(), value.getTransaction().toJson());
    }
}
