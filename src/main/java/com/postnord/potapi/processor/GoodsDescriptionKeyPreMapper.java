package com.postnord.potapi.processor;

import com.postnord.potapi.rule.model.CustomsRulesModel;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;

public class GoodsDescriptionKeyPreMapper implements KeyValueMapper<String, CustomsRulesModel, KeyValue<String, CustomsRulesModel>> {
    @Override
    public KeyValue<String, CustomsRulesModel> apply(String key, CustomsRulesModel value) {
        return new KeyValue<>("ALL", value);
    }
}
