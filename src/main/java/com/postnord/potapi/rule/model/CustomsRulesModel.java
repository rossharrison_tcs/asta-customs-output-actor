package com.postnord.potapi.rule.model;

import com.postnord.orm.service.transaction.model.TransactionResult;
import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.model.types.CustomsOriginal;
import com.postnord.potapi.model.types.ServiceCode;

import java.util.List;

public class CustomsRulesModel {

	private Transaction transaction;
	private boolean isCustomsCleared;
	private String currencyCode;
	private String goodsDescription;
	private String senderName;
	private double currencyValue;
	private boolean isGift;
	private String reciverName;
	private String getDespCountry;
	private boolean isDocument;
	private boolean isCommercialReceipient;
	private com.postnord.orm.service.transaction.model.TransactionResult ornTransaction;
	private String localCode;
	private List<ServiceCode> additionalServiceCode;
	private String serviceCode;
	private boolean prepaidValue;
	private String pnImportCategory;
	private CustomsOriginal customsOriginal;
	private boolean isPresentInCurrencyTopic;
	private boolean isPresentInNglpCUrrencyTopic;

	public CustomsRulesModel(Transaction transaction, boolean isCustomsCleared, String currencyCode,
							 String goodsDescription, String senderName, double currencyValue, boolean isGift, String reciverName,
							 String getDespCountry, boolean isDocument, boolean isCommercialReceipient, List<ServiceCode> additionalServiceCode,
							 boolean prepaidValue, CustomsOriginal customsOriginal, com.postnord.orm.service.transaction.model.TransactionResult ormTransaction,
							 String localCode, String pnImportCategory, boolean isPresentInCurrencyTopic, boolean isPresentInNglpCUrrencyTopic) {
		super();
		this.transaction = transaction;
		this.isCustomsCleared = isCustomsCleared;
		this.currencyCode = currencyCode;
		this.goodsDescription = goodsDescription;
		this.senderName = senderName;
		this.currencyValue = currencyValue;
		this.isGift = isGift;
		this.reciverName = reciverName;
		this.getDespCountry = getDespCountry;
		this.isDocument = isDocument;
		this.isCommercialReceipient = isCommercialReceipient;
		this.additionalServiceCode = additionalServiceCode;
		this.prepaidValue = prepaidValue;
		this.customsOriginal = customsOriginal;
		this.ornTransaction = ormTransaction;
		this.localCode = localCode;
		this.pnImportCategory = pnImportCategory;
	}

	public boolean isPresentInCurrencyTopic() {
		return isPresentInCurrencyTopic;
	}

	public void setPresentInCurrencyTopic(boolean presentInCurrencyTopic) {
		isPresentInCurrencyTopic = presentInCurrencyTopic;
	}

	public boolean isPresentInNglpCUrrencyTopic() {
		return isPresentInNglpCUrrencyTopic;
	}

	public void setPresentInNglpCUrrencyTopic(boolean presentInNglpCUrrencyTopic) {
		isPresentInNglpCUrrencyTopic = presentInNglpCUrrencyTopic;
	}

	public String getPnImportCategory() {
		return pnImportCategory;
	}

	public void setPnImportCategory(String pnImportCategory) {
		this.pnImportCategory = pnImportCategory;
	}

	public TransactionResult getOrnTransaction() {
		return ornTransaction;
	}

	public void setOrnTransaction(TransactionResult ornTransaction) {
		this.ornTransaction = ornTransaction;
	}

	public String getLocalCode() {
		return localCode;
	}

	public void setLocalCode(String localCode) {
		this.localCode = localCode;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public CustomsOriginal getCustomsOriginal() {
		return customsOriginal;
	}

	public void setCustomsOriginal(CustomsOriginal customsOriginal) {
		this.customsOriginal = customsOriginal;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getGoodsDescription() {
		return goodsDescription;
	}

	public void setGoodsDescription(String goodsDescription) {
		this.goodsDescription = goodsDescription;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public boolean isCustomsCleared() {
		return isCustomsCleared;
	}

	public void setCustomsCleared(boolean isCustomsCleared) {
		this.isCustomsCleared = isCustomsCleared;
	}

	public double getCurrencyValue() {
		return currencyValue;
	}

	public void setCurrencyValue(double currencyValue) {
		this.currencyValue = currencyValue;
	}

	public boolean isGift() {
		return isGift;
	}

	public void setGift(boolean isGift) {
		this.isGift = isGift;
	}

	public String getReciverName() {
		return reciverName;
	}

	public void setReciverName(String reciverName) {
		this.reciverName = reciverName;
	}

	public String getGetDespCountry() {
		return getDespCountry;
	}

	public void setGetDespCountry(String getDespCountry) {
		this.getDespCountry = getDespCountry;
	}

	public boolean isDocument() {
		return isDocument;
	}

	public void setDocument(boolean isDocument) {
		this.isDocument = isDocument;
	}

	public boolean isCommercialReceipient() {
		return isCommercialReceipient;
	}

	public List<ServiceCode> getAdditionalServiceCode() {
		return additionalServiceCode;
	}

	public void setAdditionalServiceCode(List<ServiceCode> additionalServiceCode) {
		this.additionalServiceCode = additionalServiceCode;
	}

	public void setCommercialReceipient(boolean isCommercialReceipient) {
		this.isCommercialReceipient = isCommercialReceipient;
	}

	public boolean isPrepaidValue() {
		return prepaidValue;
	}

	public void setPrepaidValue(boolean prepaidValue) {
		this.prepaidValue = prepaidValue;
	}

	@Override
	public String toString() {
		return "CustomsRulesModel [transaction=" + transaction + ", isCustomsCleared=" + isCustomsCleared
				+ ", currencyCode=" + currencyCode + ", goodsDescription=" + goodsDescription + ", senderName="
				+ senderName + "]";
	}

}
