package com.postnord.potapi.processor.internal;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;


public class CurrencyReader {
	public static final String SAMPLE_XLSX_FILE_PATH = "sample-xlsx-file.xlsx";
	private Map<String, Double> currencyRateMap = new HashMap<>();

	@PostConstruct
	public void init() throws  InvalidFormatException, IOException {
		loadCurrency();
	}
	
	public Double get(String key) {

		return currencyRateMap.get(key);
	}
	
	private void loadCurrency() throws  InvalidFormatException, IOException {

		Workbook workbook = WorkbookFactory.create(new File(SAMPLE_XLSX_FILE_PATH));

		for (Sheet sheet : workbook) {
			if (sheet.getLastRowNum() != 0) {
				for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {

					CurrencyRates currencyRate = new CurrencyRates();
					Row row = sheet.getRow(i);
					for (int j = row.getFirstCellNum(); j <= row.getLastCellNum(); j++) {
						Cell cell = row.getCell(j);
						if (j == 0) {
							currencyRate.setCurrencyCode(cell.getStringCellValue());
						}
						if (j == 3 && CellType.NUMERIC.equals(cell.getCellTypeEnum())) {
							currencyRate.setRate(cell.getNumericCellValue());
						}
						if (j == 3 && CellType.STRING.equals(cell.getCellTypeEnum())) {
							currencyRate.setRate(Double.parseDouble(cell.getStringCellValue()));
						}
					}
					
					currencyRateMap.put(currencyRate.getCurrencyCode(), currencyRate.getRate());

				}

			}

		}
		workbook.close();

	}
	
		
	}
	


