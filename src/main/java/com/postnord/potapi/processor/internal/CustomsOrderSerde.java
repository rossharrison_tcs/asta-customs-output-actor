package com.postnord.potapi.processor.internal;

import com.postnord.potapi.model.types.PnItemId;
import com.postnord.potapi.model.types.TransactionId;
import com.postnord.potapi.model.types.Version;
import com.postnord.potapi.rule.service.model.*;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.nio.charset.StandardCharsets;
import java.util.Map;

public class CustomsOrderSerde implements Serde<CustomsOrder> {
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
    }

    @Override
    public void close() {
    }

    @Override
    public Serializer<CustomsOrder> serializer() {
        return new CustomsOrderSerializer();
    }

    @Override
    public Deserializer<CustomsOrder> deserializer() {
        return new CustomsOrderDeserializer();
    }

    private class CustomsOrderSerializer implements Serializer<CustomsOrder> {
        @Override
        public void configure(Map<String, ?> configs, boolean isKey) {
        }

        @Override
        public byte[] serialize(String topic, CustomsOrder data) {
            return data.serialize();
        }

        @Override
        public void close() {
        }
    }

    private class CustomsOrderDeserializer implements Deserializer<CustomsOrder> {
        @Override
        public void configure(Map<String, ?> configs, boolean isKey) {
        }

        @Override
        public CustomsOrder deserialize(String topic, byte[] data) {
            String customsOrder = new String(data, StandardCharsets.UTF_8);

            String[] parts = customsOrder.split("\t");

            PnItemId pnItemId = new PnItemId(parts[0]);

            Size size = getSize(parts);
            ConsigneeInfoExists consigneeInfoExists = getConsigneeInfoExists(parts);
            CustomsClassification classification = getCustomsClassification(parts);

            TransactionId transactionId = new TransactionId(parts[7]);
            Version currentItemVersion = getVersion(parts, 8);
            Version currentOrderVersion = getVersion(parts, 9);


            return new CustomsOrder(pnItemId, size, consigneeInfoExists, classification, transactionId, currentItemVersion, currentOrderVersion);
        }

        private Size getSize(String[] parts) {
            String size = parts[1];

            if ("0".equals(size)) {
                return Size.UNKNOWN;
            }

            if ("1".equals(size)) {
                return Size.SMALL;
            }

            if ("2".equals(size)) {
                return Size.LARGE;
            }

            throw new RuntimeException("Unknown size value <" + size + ">");
        }

        private ConsigneeInfoExists getConsigneeInfoExists(String[] parts) {
            String consigneeInfoExists = parts[2];
            return new ConsigneeInfoExists("1".equals(consigneeInfoExists));
        }

        private CustomsClassification getCustomsClassification(String[] parts) {
            String prepaidVatStr = parts[3];
            PrepaidVat prepaidVat = new PrepaidVat("1".equals(prepaidVatStr));

            String customsDocumentStr = parts[4];
            CustomsDocument customsDocument = new CustomsDocument("1".equals(customsDocumentStr));


            String commercialRecipientStr = parts[5];
            CommercialRecipient commercialRecipient = new CommercialRecipient("1".equals(commercialRecipientStr));

            String categoryStr = parts[6];
            Category category = Category.valueOf(categoryStr);

            return new CustomsClassification(prepaidVat, customsDocument, commercialRecipient, category);
        }

        private Version getVersion(String[] parts, int position) {
            if (!parts[position].isEmpty()) {
                int version = Integer.parseInt(parts[position]);
                return new Version(version);
            }

            return null;
        }

        @Override
        public void close() {
        }
    }
}