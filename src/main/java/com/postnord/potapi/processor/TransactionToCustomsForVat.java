package com.postnord.potapi.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.kafka.streams.kstream.ValueMapper;

import com.postnord.potapi.model.Item;
import com.postnord.potapi.model.Order;
import com.postnord.potapi.model.Payload;
import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.model.types.ItemId;
import com.postnord.potapi.model.types.PnItemId;
import com.postnord.potapi.model.types.TransactionId;
import com.postnord.potapi.model.types.Version;
import com.postnord.potapi.processor.internal.ConsigneeInfoExists;
import com.postnord.potapi.processor.internal.CustomsOrder;
import com.postnord.potapi.processor.internal.Size;
import com.postnord.potapi.rule.service.filter.VATFilter;
import com.postnord.potapi.rule.service.model.Category;
import com.postnord.potapi.rule.service.model.CommercialRecipient;
import com.postnord.potapi.rule.service.model.CustomsClassification;
import com.postnord.potapi.rule.service.model.CustomsDocument;
import com.postnord.potapi.rule.service.model.PrepaidVat;

public class TransactionToCustomsForVat implements ValueMapper<com.postnord.potapi.model.Transaction, List<CustomsOrder>> {
	private VATFilter vatfilter;
	
	public TransactionToCustomsForVat(VATFilter vatfilter) {
			this.vatfilter = vatfilter;
	}
	@Override
	public List<CustomsOrder> apply(Transaction value) {
		 List<CustomsOrder> res = new ArrayList<>();
		 
		 if(vatfilter.shouldBeClearedForVAT(value)) {
			 
			 
			 TransactionId transactionId = value.getTransactionId();

	            Payload bundle = value.getPayload();

	            List<Order> orders = bundle.getOrders();
			 
	            for (Order order : orders) {
	                Version currentVersion = order.getCurrentVersion();
	                ConsigneeInfoExists consigneeInfoExists = getConsigneeInfoExists(order);

	                List<ItemId> itemIds = order.getItemIds();
	                for (ItemId itemId : itemIds) {
	                    Item item = getItem(itemId, bundle);

	                    if (item != null) {
	                        PnItemId pnItemId = item.getPnItemId();
	                        Version currentItemVersion = item.getCurrentVersion();
	                        Size size = getSize(item);
	                        CustomsClassification classification = getClassification();

	                        CustomsOrder customsOrder = new CustomsOrder(pnItemId, size, consigneeInfoExists, classification,
	                                transactionId, currentItemVersion, currentVersion);

	                        res.add(customsOrder);
	                    }
	                }
	            }
	        }

	        return res;
	}
	
	
    private Item getItem(ItemId itemId, Payload bundle) {
        List<Item> items = bundle.getItems();

        for (Item item : items) {
            if (item.getItemId().equals(itemId)) {
                return item;
            }
        }

        return null;
    }

    private ConsigneeInfoExists getConsigneeInfoExists(Order order) {
    		if (null != order.getConsignee()){
    			return new ConsigneeInfoExists(true);
    		}
    	return new ConsigneeInfoExists(false);
    }

    private Size getSize(Item item) {
        
        return Size.UNKNOWN;
    }
    
    
	private CustomsClassification getClassification() {
		// TODO what should we have for markers to choose the right values
		return new CustomsClassification(new PrepaidVat(false), new CustomsDocument(false),
				new CommercialRecipient(false), Category.VAT);
	}

}
