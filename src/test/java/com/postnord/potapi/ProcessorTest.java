package com.postnord.potapi;

import com.postnord.orm.service.transaction.model.TransactionResult;
import com.postnord.potapi.internal.Constants;
import com.postnord.potapi.internal.TestHelper;
import com.postnord.potapi.internal.TransactionSerde;
import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.processor.Processor;
import com.postnord.potapi.processor.internal.CustomsOrder;
import com.postnord.potapi.processor.internal.CustomsOrderSerde;
import org.apache.commons.io.FileUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

public class ProcessorTest {

    private TopologyTestDriver topologyTestDriver;

    public static final String APPLICATION_ID = "potapi-input-test";

    @Before
    public void setUp() {
        Properties props = new Properties();
        Processor processor = new Processor();
        props.put(StreamsConfig.CLIENT_ID_CONFIG, "potapi-input-test-client");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "potapi");
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, APPLICATION_ID);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(StreamsConfig.REPLICATION_FACTOR_CONFIG, 1);
        props.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class);
        props.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        Topology topology = processor.topology();
        topologyTestDriver = new TopologyTestDriver(topology, props);

    }

    @After
    public void release() {

        try {
            FileUtils.deleteDirectory(new File("/tmp/kafka-streams/" + APPLICATION_ID));
            FileUtils.forceMkdir(new File("/tmp/kafka-streams/" + APPLICATION_ID));
        } catch( Exception e) {
        }finally {
            topologyTestDriver.close();
        }
    }

    @Test
    public void shouldWriteToTwoTopic() throws Exception {

        TransactionResult transactionResult = TestHelper.getSample("transaction-commit.json");
        assertThat(transactionResult).isNotNull();
        writeEventToTopology(transactionResult);

        ProducerRecord<String, CustomsOrder> orders = readOrder();
        CustomsOrder order = orders.value();
        assertThat(order).isNotNull();

        ProducerRecord<String, Transaction> transactions = readTransaction();
        Transaction tx = transactions.value();
        assertThat(tx).isNotNull();
        assertThat(tx.getAction().getAction()).isEqualTo("Write");
        assertThat(tx.getActor().getActor()).isEqualTo("ASTA_CUSTOMS_OUTPUT");
        assertThat(tx.getRetries().getRetries()).isEqualTo(0);
        assertThat(tx.getTransactionId().getTransactionId()).isNotNull();
        assertThat(tx.getProcessingTime().getProcessingTime()).isNotNull();
        assertThat(tx.getPayload()).isNotNull();

        ProducerRecord<String, String> errors = readErrors();
        assertThat(errors).isNull();
    }

    @Test
    public void shouldWriteToErrorTopic() throws  Exception{
        TransactionResult transactionResult = TestHelper.getSample("transaction-commit-invalid-currency.json");
        assertThat(transactionResult).isNotNull();
        writeEventToTopology(transactionResult);

        ProducerRecord<String, String> errors = readErrors();
        String error = errors.value();
        assertThat(error).isNotNull();
    }

    private ProducerRecord<String, CustomsOrder> readOrder() {
        ProducerRecord<String, CustomsOrder> result = topologyTestDriver.readOutput(
                Constants.PROD_ASTA_SE_CUSTOMS_ORDER_WRITE, Serdes.String().deserializer(),
                new CustomsOrderSerde().deserializer());
        if (result == null) {
            return null;
        }
        return result;
    }

    private ProducerRecord<String, String> readErrors() {
        ProducerRecord<String, String> result = topologyTestDriver.readOutput(
                Constants.PROD_VAT2_ERROR, Serdes.String().deserializer(),
                Serdes.String().deserializer());
        if (result == null) {
            return null;
        }
        return result;
    }

    private ProducerRecord<String, Transaction> readTransaction() {
        ProducerRecord<String, Transaction> result = topologyTestDriver.readOutput(
                Constants.POTAPI_TRANSACTION_READ, Serdes.String().deserializer(),
                new TransactionSerde().deserializer());
        if (result == null) {
            return null;
        }
        return result;
    }

    private void writeEventToTopology(TransactionResult transactionResult) throws Exception {

        ConsumerRecordFactory<String, TransactionResult> factory = new ConsumerRecordFactory<>(
                Serdes.String().serializer(),
                com.postnord.orm.commons.serialization.transaction.TransactionSerde.transactionResult().serializer());

        ConsumerRecordFactory<String, String> globalTable = new ConsumerRecordFactory<>(Serdes.String().serializer(),
                Serdes.String().serializer());

        topologyTestDriver.pipeInput(TestHelper.getValues(Constants.CURENCY_LIST,Constants.PROD_VAT2_CURRENCY_FILTER));
        topologyTestDriver.pipeInput(TestHelper.getValues(Constants.DESCRIPTION_LIST,Constants.PROD_VAT2_DESCRIPTION_FILTER));
        topologyTestDriver.pipeInput(TestHelper.getValues(Constants.SENDER_LIST,Constants.PROD_VAT2_SENDER_FILTER));
        topologyTestDriver.pipeInput(TestHelper.getValues(Constants.GLP_CURENCY_LIST,Constants.PROD_VAT2_NGLP_CURRENCY_FILTER));
        topologyTestDriver.pipeInput(globalTable.create(Constants.PROD_VAT2_DESCRIPTION_PRE_FILTER, "ALL",
                Constants.PRE_FILTER_TEXT));
        topologyTestDriver.pipeInput(factory.create(Constants.TRANSACTION_COMMIT,
                transactionResult.getId().toString(), transactionResult));
    }
}
