package com.postnord.potapi.internal;

public class Constants {

    public static final String GLP_CURENCY_LIST = "glp-currency-list.properties";
    public static final String PROD_VAT2_NGLP_CURRENCY_FILTER = "prod-vat2-nglp-currency-filter";
    public static final String PROD_ASTA_SE_CUSTOMS_ORDER_WRITE = "prod-asta-se-customs-order-write";
    public static final String TRANSACTION_COMMIT = "transaction-commit";
    public static final String POTAPI_TRANSACTION_READ = "potapi-transaction-read";
    public static final String PROD_VAT2_CURRENCY_FILTER = "prod-vat2-currency-filter";
    public static final String PROD_VAT2_DESCRIPTION_PRE_FILTER = "prod-vat2-description-prefilter";
    public static final String PROD_VAT2_DESCRIPTION_FILTER = "prod-vat2-description-filter";
    public static final String PROD_VAT2_SENDER_FILTER = "prod-vat2-sender-filter";
    public static final String PRE_FILTER_TEXT = "HONEY|MAGAZINE|SUPPLEMENTS|BABY FOOD|SKULL|NEEDLE|TIGER BALM|EYEDROPS|" +
            "SAKE|AMMUNITION|TABLETS|2106|LINIMENT|ALCOHOL|JERKY|HERBAL|INSECTS|MG|VITAMINS|BALM|SNUS|ML|CIG|WINE|GUN" +
            "|PIPETOBACCO|ORNAMENTAL|SHELL|CIGARETTES|ANIMAL|IVORY|FIEWORKS|E-UICE|SNUFF|BEER|SIG|SEEDS|NUTRITION|BEEF|" +
            "CHEESE|MILK|E-LIQUID|PLANT|HANDMADE|SHISHA|CBD|SYRINGE|CAPSULES|DROSOPHILA MELONGASTER|INK|CHEW|BENGALS|MEDICINE" +
            "|ESSENCE|VAPORIZER|BONE|FLAVOR|TATTOO|HEALTHCARE|WHISKEY|FUR|AIRSOFT|HOYA|NUTRITIONAL|DAIRY|MEAT|FRUIT|CIGARS|" +
            "WEAPON|FOOD|HONEY|MAGAZINE|SUPPLEMENS|BABY FOOD|SKULL|NEEDLE|TIGER BALM|EYEDROPS|SAKE|AMMUNITION|TABLETS|2106|" +
            "LINIMENT|ALCOHOL|JERKY|HERBAL|INSECTS|MG|VITAMINS|BALM|SNUS|ML|CIG|WINE|GUN|PIPE TOBACCO|ORNAMENTAL|SHELL|" +
            "CIGARETTES|ANIMAL|IVORY|FIEWORKS|E-JUICE|SNUFF|BEER|SIG|SEEDS|NUTRITION|BEEF|CHEESE|MILK|E-LIQUID|PLANT|" +
            "HANDMADE|SHISHA|CBD|SYRINGE|CAPSULES|DROSOPHILAMELONGASTER|INK|CHEW|BENGALS|MEDICINE|ESSENCE|VAPORIZER|BONE|" +
            "FLAVOR|TATTOO|HEALTHCARE|WHISKEY|FUR|AIRSOFT|HOYA|NUTRITIONAL";

    public static final String PROD_VAT2_ERROR = "prod-vat2-error";

    public static final String CURENCY_LIST = "currency-list.properties";
    public static final String DESCRIPTION_LIST = "description-list.properties";
    public static final String SENDER_LIST = "sender-list.properties";

}
