package com.postnord.potapi.processor;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;

import com.postnord.orm.commons.serialization.transaction.TransactionSerde;
import com.postnord.orm.service.transaction.model.TransactionResult;
import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.processor.internal.CustomsOrder;
import com.postnord.potapi.processor.internal.CustomsOrderKeyMapper;
import com.postnord.potapi.processor.internal.CustomsOrderSerde;
import com.postnord.potapi.processor.internal.CustomsRulesModelMapper;
import com.postnord.potapi.processor.internal.TransactionKeyMapper;
import com.postnord.potapi.processor.internal.TransactionShouldBeClearedInCustoms;
import com.postnord.potapi.processor.internal.TransactionToCustomsOrder;
import com.postnord.potapi.rule.model.CustomsRulesModel;
import com.postnord.potapi.rule.service.filter.CountryFilter;
import com.postnord.potapi.rule.service.filter.CustomsRules;
import com.postnord.potapi.rule.service.filter.OriginCountryFilter;
import com.postnord.potapi.rule.service.model.Category;
import com.postnord.potapi.util.KafkaUtil;
import com.postnord.potapi.util.SystemUtil;

public class Processor {
	private static final String APPLICATION_ID = "asta-customs-output-actor";

	public void process() {
		KafkaStreams kafkaStreams = new KafkaStreams(topology(), config());
		kafkaStreams.start();
	}

	public Topology topology() {
		StreamsBuilder builder = new StreamsBuilder();

		KStream<String, CustomsRulesModel> potapiTransactions = builder.stream(inputTopic(), consumeTransactionResult())
				.filter(isNull()).filter(sourceSystemFilter()).mapValues(ormToCustomsRulesmModel()).filter(isCustomsAvailable());
		//.filter(applyCountryFilter());

		final GlobalKTable<String, String> currencyFilter = getCurrencyFilterTable(builder);

		final GlobalKTable<String, String> glpCurrencyFilter = getGlpCurrencyFilterTable(builder);

		final GlobalKTable<String, String> goodsDescriptionFilter = getDescriptionFilterTable(builder);

		final GlobalKTable<String, String> goodsDescriptionPreFilter = getDescriptionPreFilterTable(builder);

		final GlobalKTable<String, String> senderNameFilter = getSenderNameFilterTable(builder);

		// Join With Currency Filter and Determine Customs to be applied or Not
		KStream<String, CustomsRulesModel> currencyRuleApplied = potapiTransactions
				.map(currencyKeyMapper())
				.leftJoin(currencyFilter, (currencyCode, customsRulesModel) -> currencyCode,
						(customsRulesModel, currencyValue) -> {

							if(currencyValue != null){
								customsRulesModel.setPresentInCurrencyTopic(true);
							}

							if(customsRulesModel.isDocument()){
								customsRulesModel.setServiceCode(Category.GIFT.toString());
							}

							if (currencyValue != null &&  customsRulesModel.getCurrencyValue() > 0) {
								double currenyValueSek = Double.valueOf(currencyValue).doubleValue()
										* customsRulesModel.getCurrencyValue();

								if(customsRulesModel.isGift()){
									if (currenyValueSek < 500){
										customsRulesModel.setServiceCode(Category.GIFT.toString());
									}else if(currenyValueSek >= 1600){
										customsRulesModel.setCustomsCleared(false);
										customsRulesModel.setServiceCode(Category.CUSTOMS.toString());
									}else{
										customsRulesModel.setServiceCode(Category.VAT.toString());
									}
								}else if(customsRulesModel.isDocument()){
									customsRulesModel.setServiceCode(Category.GIFT.toString());
								}else if(currenyValueSek >= 1600){
									customsRulesModel.setCustomsCleared(false);
									customsRulesModel.setServiceCode(Category.CUSTOMS.toString());
								}else{
									customsRulesModel.setServiceCode(Category.VAT.toString());
								}
							}
							if(customsRulesModel.isCommercialReceipient()){
								customsRulesModel.setCustomsCleared(false);
								customsRulesModel.setServiceCode(Category.CUSTOMS.toString());
							}
							return customsRulesModel;
						})
				.map(customRulesToTransactionKey());

		// Join With nglp currency Filter and Determine currency validation to be applied or Not
		KStream<String, CustomsRulesModel> glpCurrencyCheckApplied = currencyRuleApplied
				.map(currencyKeyMapper())
				.leftJoin(glpCurrencyFilter, (currencyCode, currencyCheckModel) -> currencyCode,
						(customsRulesModel, currencyValue) ->{
							if(currencyValue != null){
								customsRulesModel.setPresentInNglpCUrrencyTopic(true);
							}
							return customsRulesModel;
						})
				.map(customRulesToTransactionKey());

		// Join With Description pre Filter and Determine Customs to be applied or Not
		KStream<String, CustomsRulesModel> preDescriptionRuleApplied = glpCurrencyCheckApplied
				.map(goodsDescriptionKeyPreMapper())
				.leftJoin(goodsDescriptionPreFilter,(preDescriptionkey, customsRulesModel) -> preDescriptionkey,
						(customsRulesModel, description) ->{
							if(description != null  && customsRulesModel.getGoodsDescription() != null){

								List<String> descriptions = Arrays.asList(description.split("\\|"));
                                if(!descriptions.contains(customsRulesModel.getGoodsDescription())){
									for(String desc : descriptions){
										if(customsRulesModel.getGoodsDescription().matches("(.*)" + desc + "(.*)")){
											customsRulesModel.setGoodsDescription(desc);
											break;
										}
									}

                                }
							}
							return customsRulesModel;
						})
				.map(customRulesToTransactionKey());
		// Join With Description Filter and Determine Customs to be applied or Not
		KStream<String, CustomsRulesModel> descriptionRuleApplied = preDescriptionRuleApplied
				.map(goodsDescriptionKeyMapper())
				.leftJoin(goodsDescriptionFilter, (goodsDescriptionKey, customsRulesModel) -> goodsDescriptionKey,
						(customsRulesModel, goodsDescription) -> {
							if (goodsDescription != null) {
								String[] descriptionArray = goodsDescription.split("\\|");
								String despCountryCode = descriptionArray[0];
								String goodsDescriptionValue = descriptionArray[1];
								String senderName = descriptionArray[2];
								String recieverName = descriptionArray[3];
								if (StringUtils.equalsIgnoreCase(goodsDescriptionValue,
										customsRulesModel.getGoodsDescription())) {
									if (StringUtils.equalsIgnoreCase(despCountryCode,
											customsRulesModel.getGetDespCountry())
											|| StringUtils.equalsIgnoreCase("*", despCountryCode)) {
										if (StringUtils.equalsIgnoreCase(senderName, customsRulesModel.getSenderName())
												|| StringUtils.equalsIgnoreCase("*", senderName)) {
											if (StringUtils.equalsIgnoreCase(recieverName,
													customsRulesModel.getReciverName())
													|| StringUtils.equalsIgnoreCase("*", recieverName)) {
												customsRulesModel.setCustomsCleared(false);
												customsRulesModel.setServiceCode(Category.RESTRICTED.toString());
											}
										}
									}
								}
							}
							return customsRulesModel;
						})
				.map(customRulesToTransactionKey());
		// Join With Sender Name Filter and Determine Customs to be applied or Not
		KStream<String, CustomsRulesModel> senderFilterApplied = descriptionRuleApplied.map(senderNameKeyMapper())
				.leftJoin(senderNameFilter, (senderNameFilterKey, customsRulesModel) -> senderNameFilterKey,
						(customsRulesModel, senderNameDescriptions) -> {
							if (senderNameDescriptions != null) {
								String[] descriptionArray = senderNameDescriptions.split("|");
								String despCountryCode = descriptionArray[0];
								String recieverName = descriptionArray[3];
								String description = descriptionArray[1];
								if (StringUtils.equalsIgnoreCase(senderNameDescriptions,
										customsRulesModel.getSenderName())) {
									if (StringUtils.equalsIgnoreCase(despCountryCode,
											customsRulesModel.getGetDespCountry())
											|| StringUtils.equalsIgnoreCase("*", despCountryCode)) {
										if (StringUtils.equalsIgnoreCase(description,
												customsRulesModel.getGoodsDescription())
												|| StringUtils.equalsIgnoreCase("*", description)) {
											if (StringUtils.equalsIgnoreCase(recieverName,
													customsRulesModel.getReciverName())
													|| StringUtils.equalsIgnoreCase("*", recieverName)) {
												customsRulesModel.setCustomsCleared(false);
												customsRulesModel.setServiceCode(Category.RESTRICTED.toString());
											}
										}
									}
								}
							}
							return customsRulesModel;
						})
				.map(customRulesToTransactionKey());

		final KStream<String, CustomsRulesModel>[] streamBranches =
				senderFilterApplied.branch( (key, customsRulesModel) ->
						!(customsRulesModel.isPresentInCurrencyTopic() || customsRulesModel.isPresentInNglpCUrrencyTopic()));

		streamBranches[0]
				.map(currencyRuleModelToErrorMapper()).to(errorTopic(), produceError());

		senderFilterApplied.flatMapValues(transactionToCustomsOrder()).map(
				pnItemIdToKey()).to(astaOutputTopic(), produceCustomsOrder());

		senderFilterApplied.map(transactionToKey()).to(ormOutputTopic(),
				produceTransaction());

		return builder.build();
	}

	private KeyValueMapper<String, CustomsRulesModel, KeyValue<String, String>> currencyRuleModelToErrorMapper() {
		return new CurrencyRuleModelToErrorMapper();
	}

	private String errorTopic() {
		return SystemUtil.getEnvironmentVariable("ERROR_TOPIC", "prod-vat2-error");
	}

	private Produced<String, String> produceError() {
		Serde<String> keySerde = Serdes.String();
		Serde<String> valueSerde = Serdes.String();
		return Produced.with(keySerde, valueSerde);
	}

	private Predicate<String, CustomsRulesModel> applyPrepaidFilter() {
		return (k, v) -> !v.isPrepaidValue();
	}

	private Predicate<String, TransactionResult> sourceSystemFilter() {
		return (key,
				transactionResult) -> ((null != transactionResult.getTransaction().getBundle().getItems() && !transactionResult.getTransaction().getBundle().getItems().isEmpty()) &&
				(transactionResult.getTransaction().getBundle().getItems().get(0).getSource()
				.toString().equalsIgnoreCase("POP")
				|| transactionResult.getTransaction().getBundle().getItems().get(0).getSource().toString()
				.equalsIgnoreCase("POR")
				|| transactionResult.getTransaction().getBundle().getItems().get(0).getSource().toString()
				.equalsIgnoreCase("POTAPI/GLP")));
	}

	private GlobalKTable<String, String> getSenderNameFilterTable(StreamsBuilder builder) {
		return builder.globalTable(senderNameFilterTopic(),
				Consumed.<String, String>with(Serdes.String(), Serdes.String()));
	}

	private String senderNameFilterTopic() {
		return SystemUtil.getEnvironmentVariable("SENDER_NAME_FILTER_TOPIC", "prod-vat2-sender-filter");
	}

	private Properties config() {
		return KafkaUtil.getStreamsApplicationProperties(APPLICATION_ID);
	}

	private String inputTopic() {
		return SystemUtil.getEnvironmentVariable("INPUT_TOPIC", "transaction-commit");
}

	private String currencyFilterTopic() {
		return SystemUtil.getEnvironmentVariable("CURRENCY_FILTER_TOPIC", "prod-vat2-currency-filter");
	}

	private String glpCurrencyFilterTopic() {
		return SystemUtil.getEnvironmentVariable("GLP_CURRENCY_FILTER_TOPIC", "prod-vat2-nglp-currency-filter");
	}

	private String descriptionFilterTopic() {
		return SystemUtil.getEnvironmentVariable("GOODS_DESCRIPTION_FILTER_TOPIC", "prod-vat2-description-filter");
	}

	private String descriptionPreFilterTopic() {
		return SystemUtil.getEnvironmentVariable("GOODS_DESCRIPTION_PRE_FILTER_TOPIC", "prod-vat2-description-prefilter");
	}

	private Predicate<String, TransactionResult> isNull() {
		return (k, v) -> v != null;
	}

	private Predicate<String, CustomsRulesModel> isCustomsAvailable() {

		return (k, v) -> (v.getCustomsOriginal() != null
                && (v.getCustomsOriginal().getGoodsDescription() != null
                || v.getCustomsOriginal().getGoodsValue() != null
                || v.getCustomsOriginal().getGoodsCategory() != null
                || v.getCustomsOriginal().getVatPrepaidAmount() != null))
                || v.getOrnTransaction().getTransaction().getBundle().getItems().get(0).getSource().toString()
                .equalsIgnoreCase("POTAPI/GLP");
	}

	private ValueMapper<TransactionResult, CustomsRulesModel> ormToCustomsRulesmModel() {
		return new CustomsRulesModelMapper();
	}

	private KeyValueMapper<String, CustomsRulesModel, KeyValue<String, CustomsRulesModel>> customRulesToTransactionKey() {
		return new CustomRulesToTransactionKeyMapper();
	}

	private ValueMapper<CustomsRulesModel, List<CustomsOrder>> transactionToCustomsOrder() {
		CountryFilter filter = new OriginCountryFilter();
		return new TransactionToCustomsOrder(filter);
	}

	public Predicate<String, CustomsRulesModel> applyCountryFilter() {
		CountryFilter filter = new OriginCountryFilter();
		return new TransactionShouldBeClearedInCustoms(filter);
	}

	public KeyValueMapper<String, Transaction, KeyValue<String, Transaction>> applyCustomsRules() {
		return new CustomsRules();
	}

	private Consumed<String, TransactionResult> consumeTransactionResult() {
		Serde<String> keySerde = Serdes.String();
		Serde<TransactionResult> valueSerde = TransactionSerde.transactionResult();

		return Consumed.with(keySerde, valueSerde);
	}

	private Consumed<String, String> consumeCurrencyFilterResult() {
		Serde<String> keySerde = Serdes.String();
		Serde<String> valueSerde = Serdes.String();

		return Consumed.with(keySerde, valueSerde);
	}

	private String astaOutputTopic() {
		return SystemUtil.getEnvironmentVariable("ASTA_OUTPUT_TOPIC", "prod-asta-se-customs-order-write");
	}

	private String ormOutputTopic() {
		return SystemUtil.getEnvironmentVariable("ORM_OUTPUT_TOPIC", "potapi-transaction-read");
	}

	private KeyValueMapper<String, CustomsOrder, KeyValue<String, CustomsOrder>> pnItemIdToKey() {
		return new CustomsOrderKeyMapper();
	}

	private KeyValueMapper<String, CustomsRulesModel, KeyValue<String, String>> transactionToKey() {
		return new TransactionKeyMapper();
	}

	private KeyValueMapper<String, CustomsRulesModel, KeyValue<String, CustomsRulesModel>> currencyKeyMapper() {
		return new CurrencyKeyMapper();
	}

	private KeyValueMapper<String, CustomsRulesModel, KeyValue<String, CustomsRulesModel>> goodsDescriptionKeyMapper() {
		return new GoodsDescriptionKeyMapper();
	}

	private KeyValueMapper<String, CustomsRulesModel, KeyValue<String, CustomsRulesModel>> goodsDescriptionKeyPreMapper() {
		return new GoodsDescriptionKeyPreMapper();
	}

	private KeyValueMapper<String, CustomsRulesModel, KeyValue<String, CustomsRulesModel>> senderNameKeyMapper() {
		return new SenderNameKeyMapper();
	}

	private Produced<String, CustomsOrder> produceCustomsOrder() {
		Serde<String> keySerde = Serdes.String();
		Serde<CustomsOrder> valueSerde = new CustomsOrderSerde();

		return Produced.with(keySerde, valueSerde);
	}

	private Produced<String, String> produceTransaction() {
		Serde<String> keySerde = Serdes.String();
		Serde<String> valueSerde = Serdes.String();

		return Produced.with(keySerde, valueSerde);
	}

	private GlobalKTable<String, String> getCurrencyFilterTable(StreamsBuilder streamsBuilder) {
		return streamsBuilder.globalTable(currencyFilterTopic(), Consumed.with(Serdes.String(), Serdes.String()));
	}

	private GlobalKTable<String, String> getGlpCurrencyFilterTable(StreamsBuilder streamsBuilder) {
		return streamsBuilder.globalTable(glpCurrencyFilterTopic(), Consumed.with(Serdes.String(), Serdes.String()));
	}

	private GlobalKTable<String, String> getDescriptionFilterTable(StreamsBuilder streamsBuilder) {
		return streamsBuilder.globalTable(descriptionFilterTopic(),
				Consumed.<String, String>with(Serdes.String(), Serdes.String()));
	}

	private GlobalKTable<String, String> getDescriptionPreFilterTable(StreamsBuilder streamsBuilder) {
		return streamsBuilder.globalTable(descriptionPreFilterTopic(),
				Consumed.<String, String>with(Serdes.String(), Serdes.String()));
	}
}
