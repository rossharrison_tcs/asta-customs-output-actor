package com.postnord.potapi.rule.service.model;

import java.util.Objects;

public class CustomsClassification {
    private PrepaidVat prepaidVat;
    private CustomsDocument customsDocument;
    private CommercialRecipient commercialRecipient;
    private Category category;

    public CustomsClassification(PrepaidVat prepaidVat, CustomsDocument customsDocument,
                                 CommercialRecipient commercialRecipient, Category category) {
        this.prepaidVat = prepaidVat;
        this.customsDocument = customsDocument;
        this.commercialRecipient = commercialRecipient;
        this.category = category;
    }

    public PrepaidVat getPrepaidVat() {
        return prepaidVat;
    }

    public CustomsDocument getCustomsDocument() {
        return customsDocument;
    }

    public CommercialRecipient getCommercialRecipient() {
        return commercialRecipient;
    }

    public Category getCategory() {
        return category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomsClassification that = (CustomsClassification) o;
        return Objects.equals(prepaidVat, that.prepaidVat) &&
                Objects.equals(customsDocument, that.customsDocument) &&
                Objects.equals(commercialRecipient, that.commercialRecipient) &&
                category == that.category;
    }

    @Override
    public int hashCode() {
        return Objects.hash(prepaidVat, customsDocument, commercialRecipient, category);
    }

    @Override
    public String toString() {
        return "CustomsClassification{" +
                "prepaidVat=" + prepaidVat +
                ", customsDocument=" + customsDocument +
                ", commercialRecipient=" + commercialRecipient +
                ", category=" + category +
                '}';
    }
}
