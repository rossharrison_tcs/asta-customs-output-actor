#!/usr/bin/env bash

time (
    if ! ./gradlew clean build; then
        exit
    fi

    if ! ./gradlew clean stage -DE2E; then
        exit
    fi

    if ! git fetch; then
        exit
    fi

    if ! git rebase; then
        exit
    fi

    if ! ./gradlew clean build; then
        exit
    fi

    if ! ./gradlew clean stage -DE2E; then
        exit
    fi

    git push
)