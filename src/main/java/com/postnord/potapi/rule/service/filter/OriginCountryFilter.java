package com.postnord.potapi.rule.service.filter;

import com.postnord.potapi.model.Order;
import com.postnord.potapi.model.Payload;
import com.postnord.potapi.model.types.Country;
import com.postnord.potapi.rule.model.CustomsRulesModel;
import com.postnord.potapi.rule.service.model.ImportCategory;
import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OriginCountryFilter implements CountryFilter {
    private Set<Country> whiteList = new HashSet<>();

    public OriginCountryFilter() {
        whiteList.add(new Country("SE"));
        whiteList.add(new Country("FI"));
        whiteList.add(new Country("DK"));
        whiteList.add(new Country("NO"));
        whiteList.add(new Country("DE"));
    }

    @Override
    public boolean shouldBeClearedInCustoms(CustomsRulesModel customsRulesModel) {
        Payload payload = customsRulesModel.getTransaction().getPayload();
        List<Order> orders = payload.getOrders();
        for (Order order : orders) {
            if (hasCountry(order)) {
                Country country = order.getConsignor().getOriginal().getCountry();
                return !whiteList.contains(country);
            }
        }
        return true;
    }

    private boolean hasCountry(Order order) {
        return order != null &&
                order.getConsignor() != null &&
                order.getConsignor().getOriginal() != null &&
                order.getConsignor().getOriginal().getCountry() != null;
    }
}
