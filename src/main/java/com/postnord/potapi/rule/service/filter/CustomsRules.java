package com.postnord.potapi.rule.service.filter;

import com.postnord.potapi.model.Item;
import com.postnord.potapi.model.Payload;
import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.model.types.Customs;
import com.postnord.potapi.model.types.CustomsOriginal;
import com.postnord.potapi.model.types.GoodsDescription;
import com.postnord.potapi.model.types.Money;
import com.postnord.potapi.processor.internal.CurrencyReader;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

public class CustomsRules implements KeyValueMapper<String, Transaction, KeyValue<String, Transaction>> {
	@Override
	public KeyValue<String, Transaction> apply(String s, Transaction transaction) {
		Payload payload = transaction.getPayload();
		if (payload != null) {
			List<Item> items = payload.getItems();
			if (!items.isEmpty()) {
				Item item = items.get(0);
				if (item != null) {
					Customs customs = item.getCustoms();
					if (customs != null) {
						List<CustomsOriginal> originals = customs.getOriginals();
						if (!originals.isEmpty()) {
							CustomsOriginal customsOriginal = originals.get(0);
							if (customsOriginal != null) {
								
								GoodsDescription goodsDescription = customsOriginal.getGoodsDescription();
								if (goodsDescription != null) {

									String goodsCategory = goodsDescription.getGoodsCategory();
									if (goodsCategory != null) {
										boolean restricted = goodsCategory.equals("wine");
										if (restricted) {
											Transaction tr = new Transaction.Builder().withId("apa").build();
											return new KeyValue<>(s, tr);
										}
									}
								}
							}
						}
					}
				}
			}
		}

		return new KeyValue<>(s, transaction);
	}



}
