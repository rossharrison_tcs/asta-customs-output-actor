package com.postnord.potapi.processor;

import org.apache.kafka.streams.kstream.Predicate;

import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.rule.service.filter.VATFilter;

public class TransactionContainsVATItems implements Predicate<String, Transaction> {
	private VATFilter vatfilter;
	
	public TransactionContainsVATItems(VATFilter vatfilter) {
				this.vatfilter = vatfilter;
	}

	@Override
	public boolean test(String key, Transaction value) {
		// TODO Auto-generated method stub
		return vatfilter.shouldBeClearedForVAT(value);
	}

}
