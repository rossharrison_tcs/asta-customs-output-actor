package com.postnord.potapi.processor.internal;

public enum Size {
    UNKNOWN(0),
    SMALL(1),
    LARGE(2);

    private final int size;

   Size(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "" + size;
    }
}
