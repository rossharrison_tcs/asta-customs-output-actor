package com.postnord.potapi.processor;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;

import com.postnord.potapi.rule.model.CustomsRulesModel;

public class SenderNameKeyMapper
		implements KeyValueMapper<String, CustomsRulesModel, KeyValue<String, CustomsRulesModel>> {

	@Override
	public KeyValue<String, CustomsRulesModel> apply(String key, CustomsRulesModel value) {
		return new KeyValue<>(value.getSenderName(), value);
	}

}
