package com.postnord.potapi.rule.service.filter;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.postnord.potapi.model.Item;
import com.postnord.potapi.model.Payload;
import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.model.types.Customs;
import com.postnord.potapi.model.types.CustomsOriginal;
import com.postnord.potapi.processor.internal.CurrencyReader;

public class CurrencyRules implements VATFilter {

	private CurrencyReader currencyreader;

	@Override
	public boolean shouldBeClearedForVAT(Transaction transaction) {

		Payload payload = transaction.getPayload();
		if (payload != null) {
			List<Item> items = payload.getItems();
			if (!items.isEmpty()) {
				Item item = items.get(0);
				if (item != null) {
					Customs customs = item.getCustoms();
					if (customs != null) {
						List<CustomsOriginal> originals = customs.getOriginals();
						if (!originals.isEmpty()) {
							CustomsOriginal customsOriginal = originals.get(0);
							if (customsOriginal != null) {
								try {
									if (checkIfCandidateForVAT(customsOriginal)) {
										return true;
									}
								} catch (InvalidFormatException | IOException e) {
									return false;
								}

							}
						}
					}
				}
			}
		}

		return false;
	}

	/*
	 * @Override public KeyValue<String, Transaction> apply(String s, Transaction
	 * transaction) { Payload payload = transaction.getPayload(); if (payload !=
	 * null) { List<Item> items = payload.getItems(); if (!items.isEmpty()) { Item
	 * item = items.get(0); if (item != null) { Customs customs = item.getCustoms();
	 * if (customs != null) { List<CustomsOriginal> originals =
	 * customs.getOriginals(); if (!originals.isEmpty()) { CustomsOriginal
	 * customsOriginal = originals.get(0); if (customsOriginal != null) { try {
	 * if(checkIfCandidateForVAT(customsOriginal)) { Transaction tr = new
	 * Transaction.Builder().withId("vat").build(); return new KeyValue<>(s, tr); }
	 * } catch (InvalidFormatException | IOException e) { return new KeyValue<>(s,
	 * transaction); }
	 * 
	 * } } } } } }
	 * 
	 * return new KeyValue<>(s, transaction); }
	 */

	private boolean checkIfCandidateForVAT(CustomsOriginal customsOriginal) throws InvalidFormatException, IOException {
		double currencyValueSek = 0;
		if (null != customsOriginal.getGoodsValue() && null != customsOriginal.getGoodsValue().getCurrency()
				&& StringUtils.isNotEmpty(customsOriginal.getGoodsValue().getCurrency().getCurrencyCode())) {
			currencyValueSek = customsOriginal.getGoodsValue().getGoodsValue();
			if (!StringUtils.equalsIgnoreCase("SEK", customsOriginal.getGoodsValue().getCurrency().getCurrencyCode())) {

				double rate = currencyreader
						.get(StringUtils.upperCase(customsOriginal.getGoodsValue().getCurrency().getCurrencyCode()));
				if (rate > 0) {
					currencyValueSek = customsOriginal.getGoodsValue().getGoodsValue() * rate;
				}
			}

		}

		return ((customsOriginal.getIsGift().getGift() && currencyValueSek > 500) || currencyValueSek > 1600);
	}

}
